# Changelog

## v1.0.13 (16/11/2020)

#### Bugfixes:

- Rare issue where an incompatibility/crash would alter the dim light of the canvas for everyone, permanently. Some refactoring was done to make sure no changes are EVER done to the canvas or tokens themselves. Only the drawn light sources themselves are altered, which are never permanently saved.
- Module setting was under the wrong category "General Module Settings" 

## v1.0.12 (15/11/2020)

#### Enhancements:

- Darkvision brightness can now be configured in module settings

## v1.0.11 (14/11/2020)

#### Bugfixes:

- When having multiple tokens with darkvision as a non-GM, sometimes one of the tokes would wrongly have bright vision instead.


## v1.0.10 (14/11/2020)

#### Bugfixes:

- Darkvision was getting disabled even when global illumination was off due to the darkness treshold.

## v1.0.9 (14/11/2020)

#### Bugfixes:

- Fix for tokens without dimsight, that were emitting a small source of dim light


## v1.0.8 (14/11/2020)

#### Bugfixes:

- Fix for colored light sources becoming too bright or losing their color

## v1.0.7 (14/11/2020)

#### Bugfixes:

- Bugginess with global illumination.  Darkvision is disabled when global lighting is used, for now.

## v1.0.6 (14/11/2020)

#### Enhancements:

- Darkvision can now render parts of dim light as bright! 
- With the help of WebGL blending it is also a performance increase from the previous version.

## v1.0.5 (13/11/2020)

- No new changes, cleanup of manifest and fixing download links required a new version

## v1.0.4 (13/11/2020)

#### Bugfixes:

- Support for foundry version 0.7.7


## v1.0.3 (12/11/2020)

#### Enhancements:

- Darkvision is now 'dynamic', and will only update dim light sources within dimsight
