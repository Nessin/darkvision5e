# No longer maintained

As the release of Perfect Vision does everything this module does (and more).  This module will no longer be maintained.

Please visit https://github.com/dev7355608/perfect-vision

#
#
#



# Darkvision5e

This Foundry VTT module aims to recreate the 5E Darkvision rules

Currently it does the following for tokens which have Dimsight:
- Show dim light as bright light

You can configure the brightness of the dim vision in the in-game module settings.
Future additions might add grayscale. However, I'm not sure yet how this could be done in the current lighting system.

This module was developed in between Foundry versions 0.7.6 and 0.7.7.  Any lower versions are not guaranteed to work.

### Find any bugs?

The module is still very fresh, and I myself am not a GM so I might not know about a lot edge cases. 
So if you find a bug, please don't hesitate to tag me in the Foundry discord (@Nessin) or simply create a new issue on gitlab. Some screenshots would be greatly appreciated :)


## How it works

- Each token with a "dim vision" value actually emits dim light only visible to the player controlling it.
- This module slightly edits the 'blend mode' of this dim vision, in order to make it ADD on top of other light sources, generally making them more bright.
- With some clever choosing of the dim vision color, we can make it so that the color of dimvision + the color of dim light = the color of bright light
- To avoid bright light becoming even brighter, all bright light inside the token's vision cone is removed. And instead, a (non-blending) copy of this bright light is drawn on top.